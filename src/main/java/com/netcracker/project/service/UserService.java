package com.netcracker.project.service;

import com.netcracker.project.entity.Training;
import com.netcracker.project.entity.User;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface UserService {

    User register(User user);

    List<User> findAll();

    User findByUsername(String username);

    void delete(UUID id);

    Collection<Training> getAllTrainings(String userLogin);
}