package com.netcracker.project.service;

import com.netcracker.project.entity.Message;

import java.util.Optional;
import java.util.UUID;

public interface MessageService {

    Optional<Message> addMessage(UUID id, Message message);
}
