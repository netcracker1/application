package com.netcracker.project.service;

import com.netcracker.project.entity.Cart;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * service Cart
 */
public interface CartService {
    List<Cart> findAll();

    Optional<Cart> findUserCart(String userLogin);

    void addTraining(UUID trainingId, String name);

    void deleteTraining(UUID trainingId, String name);

    double getTotalPrice(Cart cart);

    void clear(String name);
}
