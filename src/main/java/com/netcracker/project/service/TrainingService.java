package com.netcracker.project.service;

import com.netcracker.project.entity.Training;
import com.netcracker.project.entity.dto.TrainingDTO;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TrainingService {

    Optional<Training> addTraining(TrainingDTO trainingDTO, String userLogin);

    Optional<Training> editTraining(UUID trainingId, TrainingDTO trainingDTO,
                                    String userLogin);

    List<Training> findAll();

    Optional<Training> findById(UUID trainingId);

    Optional<Training> delete(UUID id, String userLogin);

    List<Training> getUserTrainings(String userLogin);

    List<Training> getMentorTrainings(String userLogin);
}
