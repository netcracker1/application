package com.netcracker.project.service;

import com.netcracker.project.entity.Rating;
import com.netcracker.project.entity.dto.RatingDTO;

import java.util.Optional;
import java.util.UUID;

public interface RatingService {

    double getRating(UUID trainingId);

    void addRating(RatingDTO ratingDTO);

    Optional<Rating> deleteRating(RatingDTO ratingDTO, String userLogin);
}
