package com.netcracker.project.service;

import com.netcracker.project.entity.File;
import com.netcracker.project.entity.dto.FileIdentifierDTO;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FileStorageService {

    Optional<File> storeFile(MultipartFile file, String userLogin,
                             UUID trainingId, String description,
                             String name);

    List<File> storeMultipleFiles(MultipartFile[] files, String userLogin,
                                  UUID trainingId, String description,
                                  String name);

    Optional<File> getFile(FileIdentifierDTO fileIdentifierDTO);

    Resource loadFileAsResource(String fileName);

    ResponseEntity<Resource> downloadFile(FileIdentifierDTO fileIdentifierDTO);

    Optional<List<File>> listAllfromTraining(UUID trainingId, String userLogin);

    Optional<List<File>> listAll(String userLogin);

    void delete(UUID id);
}
