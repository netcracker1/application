package com.netcracker.project.service;

import com.netcracker.project.entity.Progress;
import com.netcracker.project.entity.dto.FileIdentifierDTO;

import java.util.List;
import java.util.UUID;

public interface ProgressService {

    double getProgress(FileIdentifierDTO fileIdentifierDTO,
                                   String userLogin);

    List<Progress> getAllUserProgress(String userLogin);

    double updateProgress(FileIdentifierDTO fileIdentifierDTO,
                            String userLogin);

    void deleteProgress(UUID trainingId, String userLogin);

    void deleteAllUserProgress(String userLogin);
}
