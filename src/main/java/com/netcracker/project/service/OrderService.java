package com.netcracker.project.service;

import com.netcracker.project.entity.Order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {

    Optional<Order> getOrder(String userLogin);

    Optional<Order> saveOrder(Order order, String userLogin);

    Optional<Order> deleteOrder(UUID orderId, String userLogin);

    List<Order> findAll(String userLogin);

    List<Order> findAllUserOrders(UUID userId);
}
