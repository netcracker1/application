package com.netcracker.project.service.impl;

import com.netcracker.project.entity.Message;
import com.netcracker.project.repository.MessageRepository;
import com.netcracker.project.repository.UserRepository;
import com.netcracker.project.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public MessageServiceImpl(MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    public Optional<Message> addMessage(UUID id, Message message) {
        if(userRepository.findById(id).isPresent())
            message.setSender(userRepository.findById(id).get());
        return Optional.of(messageRepository.save(message));
    }
}
