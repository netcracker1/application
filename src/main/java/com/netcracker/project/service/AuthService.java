package com.netcracker.project.service;

import com.netcracker.project.entity.User;
import com.netcracker.project.entity.dto.reguest.LoginRequest;
import com.netcracker.project.entity.dto.reguest.RefreshTokenRequest;
import com.netcracker.project.entity.dto.reguest.SignUpRequest;
import com.netcracker.project.entity.dto.response.JwtAuthenticationResponse;
import org.springframework.http.ResponseEntity;

public interface AuthService {

    User signUp(SignUpRequest signUpRequest);

    JwtAuthenticationResponse logIn(LoginRequest loginRequest);

    ResponseEntity<JwtAuthenticationResponse> refreshAccessToken(RefreshTokenRequest refreshTokenRequest);
}
