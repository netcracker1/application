package com.netcracker.project.repository;

import com.netcracker.project.entity.Role;
import com.netcracker.project.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Role findByName(RoleName name);
}

