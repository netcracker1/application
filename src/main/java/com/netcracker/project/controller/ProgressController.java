package com.netcracker.project.controller;

import com.netcracker.project.entity.Progress;
import com.netcracker.project.entity.dto.FileIdentifierDTO;
import com.netcracker.project.service.ProgressService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/progress")
public class ProgressController {

    private final ProgressService progressService;

    ProgressController(ProgressService progressService) {
        this.progressService = progressService;
    }

    @GetMapping
    public ResponseEntity<Double> getProgress(@RequestBody FileIdentifierDTO fileIdentifierDTO,
                                                Principal principal) {
        return ResponseEntity.ok(progressService
                .getProgress(fileIdentifierDTO, principal.getName()));
    }

    @GetMapping("/all")
    public List<Progress> getAllUserProgress(Principal principal) {
        return progressService.getAllUserProgress(principal.getName());
    }

    @PutMapping("/update")
    public ResponseEntity<Double> updateProgress(@RequestBody FileIdentifierDTO fileIdentifierDTO,
                                                   Principal principal) {
        return ResponseEntity.ok(progressService
                .updateProgress(fileIdentifierDTO, principal.getName()));
    }

    @DeleteMapping("/delete/{trainingId}")
    public ResponseEntity<String> deleteProgress(@PathVariable UUID trainingId,
                                                   Principal principal) {
        progressService.deleteProgress(trainingId, principal.getName());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/deleteAll")
    public ResponseEntity<String> deleteAllProgress(Principal principal) {
        progressService.deleteAllUserProgress(principal.getName());
        return ResponseEntity.ok().build();
    }
}
