package com.netcracker.project.controller;

import com.netcracker.project.entity.Message;
import com.netcracker.project.repository.MessageRepository;
import com.netcracker.project.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/messages")
public class MessageController {
    private final MessageRepository messageRepository;
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageRepository messageRepository,
                             MessageService messageService) {
        this.messageRepository = messageRepository;
        this.messageService = messageService;
    }

    @GetMapping
    public Iterable<Message> getMessages(){
        return messageRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Message> getMessage(@PathVariable("id") UUID id){
        return messageRepository.findById(id).map(ResponseEntity::ok).orElseGet(()
                -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PostMapping("/{sender_id}")
    public ResponseEntity<?> addMessage(@RequestBody Message message, @PathVariable("sender_id") UUID id) {
        messageService.addMessage(id, message);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> deleteMessage(@PathVariable("id") UUID id) {
        messageRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
