package com.netcracker.project.controller;

import com.netcracker.project.entity.Rating;
import com.netcracker.project.entity.dto.RatingDTO;
import com.netcracker.project.service.RatingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping("/rating")
public class RatingController {

    private final RatingService ratingService;

    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }


    @GetMapping("/{trainingId}")
    public double getRating(@PathVariable UUID trainingId) {
        return ratingService.getRating(trainingId);
    }

    @PostMapping("/add")
    public ResponseEntity<Rating> addRating(@RequestBody RatingDTO ratingDTO) {
        ratingService.addRating(ratingDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Rating> deleteRating(@RequestBody RatingDTO ratingDTO,
                                               Principal principal) {

        return ratingService.deleteRating(ratingDTO, principal.getName())
                .map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.FORBIDDEN).build());
    }
}
