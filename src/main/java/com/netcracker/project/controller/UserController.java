package com.netcracker.project.controller;

import com.netcracker.project.entity.User;
import com.netcracker.project.entity.dto.UserIdentityAvailability;
import com.netcracker.project.exception.ResourceNotFoundException;
import com.netcracker.project.repository.UserRepository;
import com.netcracker.project.security.CurrentUser;
import com.netcracker.project.security.UserPrincipal;
import com.netcracker.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping
    public Iterable<User> getUsers(){
        return userService.findAll();
    }

    @GetMapping("/{username}")
    public User getUser(@PathVariable("username") String username){
        return userService.findByUsername(username);
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody User user) {
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") UUID id) {
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/me")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("UserInfo", "id", userPrincipal.getId()));
    }

    @GetMapping("/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        return new UserIdentityAvailability(!userRepository.existsByUsername(username));
    }

    @GetMapping("/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        return new UserIdentityAvailability(!userRepository.existsByEmail(email));
    }

    @PostMapping("/blockOrUnblock")
    public ResponseEntity<User> blockOrUnblock(@RequestBody String email) {
        User user;
        if (userRepository.findByEmail(email).isPresent()) {
            user = userRepository.findByEmail(email).get();
            user.setIsblock(!user.getIsblock());
            userRepository.save(user);
        }
        return ResponseEntity.ok().build();
    }
}
