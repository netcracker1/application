package com.netcracker.project.controller;

import com.netcracker.project.entity.File;
import com.netcracker.project.entity.dto.FileIdentifierDTO;
import com.netcracker.project.service.FileStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/files")
public class FileController {

    private final FileStorageService fileStorageService;

    public FileController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("/uploadFile/{trainingId}")
    public ResponseEntity<File> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("name") String name,
                                           @RequestParam("description") String description, Principal principal,
                                           @PathVariable UUID trainingId) {

        return fileStorageService.storeFile(file, principal.getName(), trainingId, description, name)
                .map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.FORBIDDEN).build());

    }

    @PostMapping("/uploadMultipleFiles/{trainingId}")
    public List<File> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files, @RequestParam("name") String name,
                                          @RequestParam("description") String description, Principal principal,
                                          @PathVariable UUID trainingId) {

        return fileStorageService.storeMultipleFiles(files, principal.getName(), trainingId, description, name);
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping
    public ResponseEntity<List<File>> getFiles(Principal principal){

        return fileStorageService.listAll(principal.getName())
                .map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.FORBIDDEN).build());
    }

    @GetMapping("/{trainingId}")
    public ResponseEntity<List<File>> getFilesFromTraining(@PathVariable UUID trainingId,
                                           Principal principal) {

        return fileStorageService.listAllfromTraining(trainingId, principal.getName())
                .map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.FORBIDDEN).build());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<File> deleteFile(@PathVariable("id") UUID id) {
        fileStorageService.delete(id);
        return ResponseEntity.ok().build();
    }
}
