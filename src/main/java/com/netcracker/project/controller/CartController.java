package com.netcracker.project.controller;

import com.netcracker.project.entity.Cart;
import com.netcracker.project.service.CartService;
import com.netcracker.project.service.impl.CartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/cart")
public class CartController {

    private final CartService cartService;

    @Autowired
    public CartController(CartServiceImpl cartService) {
        this.cartService = cartService;
    }

    //метод, возвращающий все корзины
    @GetMapping("/all")
    public List<Cart> getAllCarts() {
        return cartService.findAll();
    }

    //вернуть корзину пользователя
    @GetMapping
    public ResponseEntity<Cart> getUserCart(Principal principal) {
        return cartService.findUserCart(principal.getName()).map(ResponseEntity::ok).orElseGet(()
                -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PostMapping
    public ResponseEntity<String> addTrainingToUserCart(@RequestBody String trainingId,
                                                        Principal principal){
        cartService.addTraining(UUID.fromString(trainingId), principal.getName());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTraining(@PathVariable("id") String trainingId,
                                                 Principal principal) {
        cartService.deleteTraining(UUID.fromString(trainingId), principal.getName());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/totalPrice")
    public ResponseEntity<?> getTotalPrice(Principal principal) {
        if(cartService.findUserCart(principal.getName()).isPresent()) {
            return ResponseEntity.ok(cartService.getTotalPrice(cartService.findUserCart(principal.getName()).get()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @DeleteMapping("/deleteAll")
    public ResponseEntity<String> deleteTraining(Principal principal) {
        cartService.clear(principal.getName());
        return ResponseEntity.ok().build();
    }
}
