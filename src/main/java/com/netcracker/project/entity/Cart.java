package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@Table(name="carts")
public class Cart {

    @Id
    private UUID id;

    @ToString.Exclude
    @JsonIgnoreProperties(value = {"cart", "trainings"}, allowSetters = true)
    @OneToOne
    @MapsId
    private User user;

    @ToString.Exclude
    @ElementCollection
    private List<Training> trainings;

    public Cart(User user) {
        this.user = user;
    }

    public Cart clear() {
        this.trainings.clear();
        return this;
    }
}
