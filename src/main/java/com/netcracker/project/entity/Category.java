package com.netcracker.project.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data @NoArgsConstructor
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED)
    UUID id;

    private String name;

    private String description;

    @ManyToMany
    @JoinTable(name="category_training",
            joinColumns=@JoinColumn(name="category_id"),
            inverseJoinColumns=@JoinColumn(name="training_id"))
    private @ToString.Exclude List<Training> trainings;
}
