package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.netcracker.project.entity.enums.AuthProvider;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
@Data @NoArgsConstructor
@Table (name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID id;

    private @NonNull String username;

    @Email
    private @NonNull String email;

    private String imageUrl;

    private Boolean isblock = false;

    @JsonIgnore
    private @NonNull String password;

    @Enumerated(EnumType.STRING)
    private @NotNull AuthProvider provider;

    private String providerId;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Progress> progress;

    @ManyToMany
    @JoinTable(name="user_role",
            joinColumns=@JoinColumn(name="user_id"),
            inverseJoinColumns=@JoinColumn(name="role_id"))
    private Collection<Role> roles;

    @JsonIgnoreProperties(value = "user", allowSetters = true)
    @OneToMany (mappedBy= "user", fetch=FetchType.EAGER)
    private List<Training> trainings;

    @ElementCollection
    private List<UUID> purchasedTrainings;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Cart cart;

    public User(String email, String username, String password,
                List<Role> roles, AuthProvider provider, Cart cart) {
        purchasedTrainings = new ArrayList<>();
        this.trainings = new ArrayList<>();
        this.email = email;
        this.roles = roles;
        this.username = username;
        this.password = password;
        this.provider = provider;
        this.cart = cart;
    }
}
