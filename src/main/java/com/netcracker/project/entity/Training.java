package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Table(name = "trainings")
public class Training {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID id;

    private String imgUrl;
    private double trainingRate;
    private @NonNull String name;
    private @NonNull String description;
    private @NonNull int price;
    private LocalDate creationDate;
    private LocalDate editDate;
    private Boolean isDelete = false;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Rating> rating;

    @JsonIgnoreProperties(value = {"trainings","cart"}, allowSetters = true)
    @ManyToOne (optional=false, fetch=FetchType.EAGER)
    @JoinColumn (name="user_id")
    @ToString.Exclude private User user;

    @ToString.Exclude
    @JsonIgnoreProperties(value = "trainings", allowSetters = true)
    @ManyToMany
    @JoinTable(name="category_training",
            joinColumns=@JoinColumn(name="training_id"),
            inverseJoinColumns=@JoinColumn(name="category_id"))
    private List<Category> categories;

    @JsonIgnoreProperties(value = "training", allowSetters = true)
    @OneToMany (mappedBy="training", cascade = CascadeType.ALL)
    private List<File> content;

    public Training(@NonNull String name, @NonNull String description,
                    int price, User user) {
        this.creationDate = LocalDate.now();
        this.editDate = this.creationDate;
        this.rating = new ArrayList<>();
        this.description = description;
        this.price = price;
        this.user = user;
        this.name = name;
    }
}
