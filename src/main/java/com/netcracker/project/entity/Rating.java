package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@JsonIgnoreProperties("training")
@Data @NoArgsConstructor
@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID id;

    private @Setter(AccessLevel.PROTECTED) UUID userId;

    @JsonIgnoreProperties({"user", "rating", "content"})
    @ManyToOne(fetch = FetchType.LAZY)
    private @ToString.Exclude Training training;

    /** 1 <= rate <= 5 */
    private double rate;

    private String review;

    public Rating(UUID userId, String review, double rate, Training training) {
        this.training = training;
        this.userId = userId;
        this.review = review;
        this.rate = rate;
    }
}
