package com.netcracker.project.entity;

public class ServiceConstants {
    public static final String[] article = new String[] {"TEXT", "TEXT/PLAIN", "/PLAIN", ".TXT", ".RTF", ".DOC", ".DOCX", ".HTML", ".PDF", ".ODT", "/OCTET-STREAM"};
    public static final String[] video = new String[] {"/MP4", "/M4V", "/MOV", "/MPG", "/MPEG", "/WEBM"};
    public static final String[] image = new String[] {"/PNG", "/JPG", "/JPEG", "/TIF", "/TIFF", "/GIF"};
}
