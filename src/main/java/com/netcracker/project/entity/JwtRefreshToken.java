package com.netcracker.project.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Data @NoArgsConstructor
@Table(name = "refresh_tokens")
public class JwtRefreshToken {
    @Id
    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    private Instant expirationDateTime;

    public JwtRefreshToken(String token) {
        this.token = token;
    }
}
