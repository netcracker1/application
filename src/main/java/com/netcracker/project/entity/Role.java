package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.project.entity.enums.RoleName;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.List;

@Entity
@Data @NoArgsConstructor
@Table (name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) Integer id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    private @NonNull RoleName name;

    @ToString.Exclude
    @JsonIgnore
    @ManyToMany
    @JoinTable(name="user_role",
            joinColumns=@JoinColumn(name="role_id"),
            inverseJoinColumns=@JoinColumn(name="user_id"))
    private List<User> users;

    public Role(RoleName name) {
        this.name = name;
    }
}
