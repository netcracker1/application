package com.netcracker.project.entity.enums;

public enum SorterDirection {
    ASC,
    DESC
}
