package com.netcracker.project.entity.enums;

public enum FilterOperator {
    EQUALS,
    IN
}
