package com.netcracker.project.entity.enums;

public enum  AuthProvider {
    local,
    google,
    github
}
