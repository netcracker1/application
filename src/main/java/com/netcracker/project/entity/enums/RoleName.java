package com.netcracker.project.entity.enums;

public enum  RoleName {
    ROLE_USER,
    ROLE_MENTOR,
    ROLE_ADMIN
}
