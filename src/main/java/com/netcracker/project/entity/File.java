package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.netcracker.project.entity.enums.ContentType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Data @NoArgsConstructor
@Table(name = "files")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID id;

    @JsonIgnoreProperties(value = {"user", "content", "rating"}, allowSetters = true)
    @ManyToOne (optional = false, fetch = FetchType.EAGER)
    @JoinColumn (name="training_id")
    private @NonNull @ToString.Exclude Training training;

    private @NonNull LocalDate uploadDate;

    private @NonNull String name;

    @Enumerated(EnumType.STRING)
    private @NonNull ContentType contentType;

    private String type;

    private @NonNull String path;

    private String description;

    private String contentName;

    public File(String name, ContentType contentType, String type,
                String path, Training training, String description,
                String contentName) {
        this.uploadDate = LocalDate.now();
        this.contentType = contentType;
        this.training = training;
        this.type = type;
        this.name = name;
        this.path = path;
        this.description = description;
        this.contentName = contentName;
    }
}
