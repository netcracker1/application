package com.netcracker.project.entity;

import com.netcracker.project.entity.enums.FilterOperator;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
public class Filter {

    @Enumerated(EnumType.STRING)
    private FilterOperator operator;

    private String property;

    private Object value;

    public Filter(List<Object> value, String property, FilterOperator operator) {
        this.operator = operator;
        this.property = property;
        this.value = value;
    }

    public Filter(Object value, String property, FilterOperator operator) {
        this.operator = operator;
        this.property = property;
        this.value = value;
    }
}
