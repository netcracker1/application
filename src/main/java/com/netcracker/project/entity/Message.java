package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED)
    UUID id;

    @Column(columnDefinition = "TEXT")
    private String text;

    private @NonNull LocalDate createDate;

    @OneToOne
    @JsonIgnoreProperties(value = "message", allowSetters = true)
    @JoinColumn(name="sender_id")
    private User sender;

    public Message(String text, @NonNull LocalDate createDate, User sender) {
        this.text = text;
        this.createDate = createDate;
        this.sender = sender;
    }
}
