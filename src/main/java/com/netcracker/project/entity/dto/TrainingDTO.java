package com.netcracker.project.entity.dto;

import com.netcracker.project.entity.Category;
import lombok.Data;

import java.util.UUID;

@Data
public class TrainingDTO {
    private UUID id;
    private String name;
    private String description;
    private int price;
    private Category category;
}
