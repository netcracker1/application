package com.netcracker.project.entity.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.util.UUID;

@Data
public class FileIdentifierDTO {
    private @Setter(AccessLevel.PROTECTED) UUID fileId;
    private @Setter(AccessLevel.PROTECTED) UUID trainingId;
}
