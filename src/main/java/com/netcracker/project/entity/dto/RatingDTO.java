package com.netcracker.project.entity.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.util.UUID;

@Data
public class RatingDTO {
    private @Setter(AccessLevel.PROTECTED) UUID Id;
    private @Setter(AccessLevel.PROTECTED) UUID ratingId;
    private @Setter(AccessLevel.PROTECTED) UUID trainingId;
    private @Setter(AccessLevel.PROTECTED) UUID userId;
    private String review;
    private double rate;
}
