package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@JsonIgnoreProperties("user")
@Data @NoArgsConstructor
@Entity
@Table(name = "progress")
public class Progress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID Id;

    private @Setter(AccessLevel.PROTECTED) UUID trainingId;

    @ManyToOne(fetch = FetchType.EAGER)
    private @ToString.Exclude User user;

    /** 0 <= progress <= 1 */
    private double progress;

    /** UUID - file id, boolean - isDone. */
    @ElementCollection
    private Map<UUID, Boolean> content;

    public Progress(User user, UUID trainingId) {
        this.trainingId = trainingId;
        this.progress = 0;
        this.user = user;
        content = new HashMap<>();
    }
}
