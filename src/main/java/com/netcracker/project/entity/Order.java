package com.netcracker.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity @Table (name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private @Setter(AccessLevel.PROTECTED) UUID id;

    @ToString.Exclude
    @ElementCollection
    private List<Training> trainings;

    @JsonIgnoreProperties(value = {"order", "cart", "trainings"}, allowSetters = true)
    @ManyToOne (optional=false, fetch = FetchType.EAGER)
    @JoinColumn (name="user_id")
    private @NonNull @ToString.Exclude User user;

    @Column(name = "order_total")
    private int orderTotal = 0;

    private LocalDate orderDate;

    private void calcOrderTotal(List<Training> trainings) {
        for (Training training : trainings) {
            orderTotal += training.getPrice();
        }
    }

    public Order(List<Training> trainings, @NonNull User user) {
        this.trainings = trainings;
        this.user = user;
        this.orderDate = LocalDate.now();
        calcOrderTotal(trainings);
    }
}
