package com.netcracker.project.entity;

import com.netcracker.project.entity.enums.SorterDirection;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class Sorter {

    @Enumerated(EnumType.STRING)
    private SorterDirection direction;

    private Object property;

    public Sorter(Object property, SorterDirection direction) {
        this.direction = direction;
        this.property = property;
    }
}
